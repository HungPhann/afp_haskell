import Data.List.Split
import Data.List
import qualified Data.IntMap.Strict as IntMap
import Data.Maybe as Maybe

main = interact main_palindrome

main_palindrome :: String -> String
main_palindrome s = show $ palindrome len y arr
    where
        arr = (init_array len)
        (x:y:z) = splitOn "\n" s
        len = (read::String->Int) x

init_array :: Int -> IntMap.IntMap (IntMap.IntMap Integer)
init_array len = init_array' 0 0 (IntMap.insert 0 IntMap.empty IntMap.empty)
    where
        init_array' :: Int -> Int -> IntMap.IntMap (IntMap.IntMap Integer) -> IntMap.IntMap (IntMap.IntMap Integer)
        init_array' i j result = if i >= len then
                                    result
                                else if j >= len then
                                    init_array' (i+1) 0 (IntMap.insert (i+1) IntMap.empty result)
                                else
                                    let
                                        x = IntMap.lookup i result
                                        y = Maybe.fromJust x
                                        z = IntMap.insert j 0 y
                                    in   
                                        init_array' i (j+1) (IntMap.insert i z result)


array_get_element_at :: IntMap.IntMap (IntMap.IntMap Integer) -> Int -> Int -> Integer
array_get_element_at arr i j = result
    where
        x = IntMap.lookup i arr
        y = Maybe.fromJust x
        z = IntMap.lookup j y
        result = if Maybe.isJust z then
                    Maybe.fromJust z
                else
                    0


array_set_element_at :: IntMap.IntMap (IntMap.IntMap Integer) -> Int -> Int -> Integer -> IntMap.IntMap (IntMap.IntMap Integer)       
array_set_element_at arr i j v = IntMap.insert i z arr
    where
        x = IntMap.lookup i arr
        y = Maybe.fromJust x
        z = IntMap.insert j v y


palindrome :: Int -> String -> IntMap.IntMap (IntMap.IntMap Integer) -> Integer
palindrome len s arr = mod (fst (countPS 0 (len-1) arr)) 20130401
    where
        countPS :: Int -> Int -> IntMap.IntMap (IntMap.IntMap Integer) -> (Integer, IntMap.IntMap (IntMap.IntMap Integer))
        countPS i j arr = 
                    let 
                        v = array_get_element_at arr i j
                    in
                        if i > j then
                            (0, arr)
                        else if v > 0 then
                            (v, arr)         
                        else if ((!!) s i) == ((!!) s j) then
                            let
                                (x, new_arr_1) = countPS (i+1) j arr
                                (y, new_arr_2) = countPS i (j-1) new_arr_1
                                z = x + y + 1
                                new_arr = array_set_element_at new_arr_2 i j z
                            in
                                (z, new_arr)

                        else
                            let
                                (x, new_arr_1) = countPS (i+1) j arr
                                (y, new_arr_2) = countPS i (j-1) new_arr_1
                                (z, new_arr_3) = countPS (i+1) (j-1) new_arr_2
                                t = x + y - z
                                new_arr = array_set_element_at new_arr_3 i j t
                            in
                                (t, new_arr)

