import qualified Data.ByteString.Lazy.Char8 as L
import qualified Data.ByteString.Lazy as LL
import qualified Data.ByteString.Char8 as B

import qualified Data.Map as Map
import Data.Map (Map)

import qualified Data.IntSet as Set
import Data.IntSet (IntSet)

import System.Environment
import System.Exit
import System.IO
import Data.Array
import Data.Char
import Control.Monad

import qualified Control.Parallel.Strategies as S 
import Control.Exception.Base

-- A document index and search program.  Use it like this:
--
-- $ ./index docs/*
-- search: <enter search term here>
-- docs/file1
-- docs/file2
-- docs/file3
--
--

-- Documents are numbered by the order they appear on the command line
type DocSet = IntSet

-- A DocIndex maps a word to the set of documents that contain the word
type DocIndex = Map B.ByteString DocSet

mkIndex :: (Int, L.ByteString) -> DocIndex
mkIndex (i, s)
  = Map.fromListWith Set.union [ (B.concat (L.toChunks w), Set.singleton i)
                               | w <- ws ]
  where ws = L.splitWith (not . isAlphaNum) s

search :: [B.ByteString] -> DocIndex -> DocSet
search words index  = foldr1 Set.intersection (map lookup words)
  where lookup w = Map.findWithDefault Set.empty w index

-- -----------------------------------------------------------------------------


main = do
  hSetBuffering stdout NoBuffering
  fs <- getArgs
  
  -- Step 1: build the index
  ss <- mapM L.readFile fs
  evaluate (length ss)
  
  let
      -- indices is a separate index for each (numbered) document
      indices :: [DocIndex]
      indices = S.parMap S.rpar mkIndex $ zip [0..] ss

      -- array mapping doc number back to filename
      arr = listArray (0,length fs - 1) fs
  
  -- Step 2: perform search
  forever $ do
    putStr "search (^D to end): "
    eof <- isEOF
    when eof $ exitWith ExitSuccess
    s <- B.getLine
    putStr "wait... "
        
    let result :: DocSet  -- set of docs containing the words in the term
        result = Set.unions $ S.parMap S.rpar (search (B.words s)) indices
        -- map the result back to filenames
        files = map (arr !) (Set.toList result)

    putStrLn ("\n" ++ unlines files)


-- ghc -o my_indexing my_indexing.hs -rtsopts -threaded && ulimit -n 4096 && echo "parallel concurrent" | ./my_indexing docs/* +RTS -N2 -s