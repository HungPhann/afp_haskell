import System.Environment (getArgs)
import System.IO (openFile, hGetContents)
import System.IO( IOMode( ReadMode ) )
import Control.Monad.State
import Data.Array.IO
import Data.List.Split

main :: IO ()
main = do
        args <- getArgs
        file <- openFile (head args) ReadMode
        text <- hGetContents file
        let (x:y:z) = splitOn "\n" text
        let len = ((read::String->Int) x)
        arr <- newArray ((0,0), (len, len)) 0 :: IO (IOArray (Int, Int) Integer)
        init_array 0 len arr
        palindrome 0 2 len y arr
        result <- readArray arr (0, len-1)
        print (mod result 20130401)


init_array :: Int -> Int -> IOArray (Int, Int) Integer -> IO ()
init_array i len arr = if i >= len then
                            return ()
                        else do
                            writeArray arr (i, i) 1
                            init_array (i+1) len arr
                

palindrome :: Int -> Int -> Int -> String -> IOArray (Int, Int) Integer -> IO ()
palindrome i j len s arr = do
    if j > len then
        return ()
    else do
        let k = i + j - 1
        if k >= len then
            palindrome 0 (j+1) len s arr
        else 
            if ((!!) s i) == ((!!) s k) then do
                x <- readArray arr (i+1, k)
                y <- readArray arr (i, k-1)
                writeArray arr (i, k) (x+y+1)
                palindrome (i+1) j len s arr
            else do
                x <- readArray arr (i+1, k)
                y <- readArray arr (i, k-1)
                z <- readArray arr (i+1, k-1)
                writeArray arr (i, k) (x+y-z)
                palindrome (i+1) j len s arr

