import Data.List.Split
import Data.List

main = interact main_amplify
-- func :: String -> String
-- func x = show $ am $ to_Int_list x


main_amplify :: String -> String
main_amplify input = show $ min_amplify list
    where
        list = to_Int_list input
        min_amplify (x:y:rest) = amplify y rest


to_Int_list :: String -> [Int]
to_Int_list s = z
    where 
        x = splitOn "\n" s
        y = intercalate " " x
        z = map (read::String->Int) (splitOn " " y)

        
amplify :: Int -> [Int] -> Int
amplify _ [] = 0
amplify m list = amplify_left + amplify_right + (ceiling ((fromIntegral (max-max_left)) / (fromIntegral m))) + (ceiling ((fromIntegral (max-max_right)) / (fromIntegral m)))
    where
        max = maximum list
        left_list = takeWhile (< max) list
        right_list = tail (dropWhile (< max) list)
        max_left = if null left_list then max else maximum left_list
        max_right = if null right_list then max else maximum right_list
        amplify_left = amplify m left_list
        amplify_right = amplify m right_list