import System.Environment (getArgs)
import System.IO (openFile, hGetContents)
import System.IO( IOMode( ReadMode ) )
import Control.Monad.State
import Data.Array.IO
import Data.List.Split

main :: IO ()
main = do
        args <- getArgs
        file <- openFile (head args) ReadMode
        text <- hGetContents file
        let (x:y:z) = splitOn "\n" text
        let len = ((read::String->Int) x)
        arr <- newArray ((0,0), (len, len)) 0 :: IO (IOArray (Int, Int) Integer)
        result <- palindrome 0 (len-1) y arr
        let final_result = mod result 20130401
        print final_result


palindrome :: Int -> Int -> String -> IOArray (Int, Int) Integer -> IO (Integer)
palindrome i j s arr = do
    x <- readArray arr (i, j)
    if x > 0
        then return x
    else if i == j then do
        writeArray arr (i, j) 1
        return 1
    else if i > j then
        return 0
    else if ((!!) s i) == ((!!) s j) then do
        y <- palindrome (i+1) j s arr
        z <- palindrome i (j-1) s arr
        let result = y + z + 1
        writeArray arr (i, j) result
        return result
    else do
        y <- palindrome (i+1) j s arr
        z <- palindrome i (j-1) s arr
        t <- palindrome (i+1) (j-1) s arr
        let result = y + z - t
        writeArray arr (i, j) result
        return result