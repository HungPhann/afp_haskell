primes :: [Integer]
primes = sieve (2 : [3, 5..])
  where
    sieve (p:xs) = p : sieve [x|x <- xs, x `mod` p > 0]

foldl' :: (a -> b -> a) -> a -> [b] -> a
foldl' op init [] = init
foldl' op init (x:xs) = 
    let 
        a = (init `op` x)
    in 
        a `seq` foldl' op a xs

lazy :: Integer -> Integer -> [Integer] -> Integer
lazy from to list = foldl' (+) 0 xs
    where
        build_list list = build_list' 0 [] list
            where
                loop = ceiling (logBase 2 (fromIntegral to + 1))
                build_list' i result (x:xs) = if i < loop
                                                then build_list' (i+1) (result ++ [x] ++ result) xs
                                            else
                                                result
        ys = build_list list
        xs = drop (fromIntegral from - 1) (take (fromIntegral to) ys)


